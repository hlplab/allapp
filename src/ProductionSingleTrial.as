import mx.controls.LinkButton;

private function productionVocabClickProductionSingle(event:Event):void {
	for(var lbi:int=0; lbi<productionVocab.length; lbi++) {
		var lb:LinkButton = productionVocab[lbi] as LinkButton;
		if(lb != event.currentTarget) lb.selected = false;
	}
	event.currentTarget.selected = true;
	ProductionAnswer = new ArrayCollection([event.currentTarget.label]);
	QueuedSounds = [Sounds[event.currentTarget.label]];
	CurrentlyPlaying = -1;
	playSounds(0);
}

protected function productionSingleTrial():void {
	var trial:Array = Trials[CurrentTrial]
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = null;
	setUpProductionVocab(new ArrayCollection([trial[2]]));
	ProductionAnswer = new ArrayCollection();
	QueuedSounds = new Array();
	// in an single word production trial, we want toggle buttons
	for(var lbi:int=0; lbi<productionVocab.length; lbi++) {
		var lb:LinkButton = productionVocab[lbi] as LinkButton;
		lb.toggle = true;
	}
	// check whether everything is loaded
	if(CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) {
		productionSingleTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
//	continueButton.enabled = canContinue();
	replayButton.enabled = replayButton.visible // since there's no sound playing to wait for
}


protected function productionSingleTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	CurrentNetStream.seek(0);
	CurrentNetStream.resume();
}

