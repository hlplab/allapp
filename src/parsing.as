import ndef.parser.*;

// actually doesn't put in variables, only special functions
protected function replaceVars(str:String):String {
	trace("replacing vars: '" + str + "'");
	var loc:int = str.indexOf('correct("');
	while(loc != -1) {
		trace(str);
		var lab:String = str.slice(loc + 9);
		var loc2:int = lab.indexOf('")');
		lab = lab.slice(0, loc2);
		str = str.slice(0, loc) + " " + CorrectInSection[lab] + " " + str.slice(loc+9+loc2+2);
		loc = str.indexOf('correct("');
	}
	loc = str.indexOf('currentTime()');
	while(loc != -1) {
		var date:Date = new Date();
		str = str.slice(0, loc) + date.valueOf() + str.slice(loc+13);
		loc = str.indexOf('currentTime()');
	}
	trace("replaced vars: '" + str + "'");
	return str;
}

protected function parseExpression(str:String):* {
	str = StringUtil.trim(str);
	trace("parsing: " + str);
	// special case for strings (for the moment, the parser should be improved)
	if(str.substr(0,1) == '"' && str.substr(str.length-1) == '"') {
		return str.substr(1, str.length-2);
	}
	if(str == "true") {
		return 1;
	}
	if(str == "false") {
		return 0;
	}
	var keys:Array = Variables.toKeyArray();
	var exp:String = replaceVars(str);
	var values:Array = [];
	var key:String;
	for each(key in keys) {
		values.push(Variables.get(key));
	}
	// now deal with cookies
	for(key in Cookie.data) {
		var value:* = Cookie.data[key];
		key = '@' + key;
		keys.push(key);
		values.push(value);
		trace(key + " = " + value);
	}
	trace("sending to parser:'" + exp + "'");
	var parser:Parser = new Parser(keys);
	return parser.eval(parser.parse(exp), values);
}

protected function setVariable(key:String, expr:String):void {
	var result:* = parseExpression(expr);
	if(key.substr(0, 1) == '@') {
		key = key.substr(1);
		Cookie.data[key] = expr;
		Cookie.flush();
	} else {
		if(Variables.hasKey(key)) {
			Variables.remove(key);
		}
		Variables.set(key, result);
		trace(Variables.toString());
	}
}

