[![License](http://img.shields.io/badge/license-BSD-red.svg)](http://choosealicense.com/licenses/bsd-2-clause/)

# Compiling the applet

Download and install the [Flex4 SDK](http://flex.apache.org/installer.html)

Then in the ```src/``` directory of the applet repository:
    
    :::bash
    # set "FLEX_LIB" to where you have the Flex4 SDK installed
    export FLEX_LIB='/usr/local/flex_sdk-4.14.1'
    # You need the latest Polygonal DS library from http://polygonal.github.com/ds/swc/polygonal-ds-latest.zip
    export DS_LIB='/usr/local/polygonal-ds-latest/lib/'
    # Also add the -compiler.advanced-telemetry if you want to use Adobe Scout for debugging
    ${FLEX_LIB}/bin/mxmlc -compiler.debug -compiler.include-libraries=${DS_LIB}/ds.swc -theme=${FLEX_LIB}/frameworks/themes/Halo/halo.swc allapp.mxml

# Layout of stimuli directory

    Base/
        crossdomain.xml
        allapp.swf
        sounds/
        trials/
        videos/

# Format of files
TODO:

  - write this
  - add screen shots

  - required video format(s?)
  - required sound format(s?)
  - format of trial file
  
  
## Trial types

### Exposure
    exp <video> <word1> .... <wordN>
  
### Discriminate
    discrim  <video1> <video2> <word1> .... <wordN>

Displays two videos and a target word. Subjects click on the video they think is correct.    
![discrim trial](discrim.jpg "Discriminate trial")
  
### TypingPrompt 
    typeprompt <video> <word>
    
### ProductionSingle
    prod1 <video> <targetword>

Displays a video and a list of up to 14 possible names (less if experiment vocabulary is smaller than 14) for the target. Possible names are the target plus words randomly sampled from experiment vocabulary. Subjects click on the names and can play the sounds for the names. Clicking "Continue" finalizes word choice. No feedback is given. 

![prod1 trial](prod1.jpg "Production single trial")
### Production
    prod <video> <word1> .... <wordN>

### CuedProduction
    cuedprod <video> <word1> .... <wordN>

### Typing
    type <video> <word1> .... <wordN>
    
### Four Pic
    4pic <img1> <img2> <img3> <img4> <sound>

Displays four images in a 2x2 grid and plays the name of the target. The first image specified is the target, but
the grid position of the images is randomized. Subjects click the picture that matches the target. The current selection
is highlighted in green. After clicking "Continue" feedback is provided in the form of a ding (correct) or discordant 
chime (incorrect) and the target image flashing.

![4pic trial](4pic.jpg "Four pic trial")