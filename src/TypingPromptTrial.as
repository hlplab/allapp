
protected function typingPromptTrial():void {
	var trial:Array = Trials[CurrentTrial]
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = null;
	typingResponse.text = '';
	typingResponse.setFocus();
	typingPromptLabel.text = Trials[CurrentTrial][2];
	ProductionAnswer = new ArrayCollection();
	QueuedSounds = new Array();
	// check whether everything is loaded
	if(CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) {
		typingPromptTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
//	continueButton.enabled = canContinue();
	replayButton.enabled = replayButton.visible // since there's no sound playing to wait for

}


protected function typingPromptTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	CurrentNetStream.seek(0);
	CurrentNetStream.resume();
}

