import flash.events.Event;
import mx.controls.LinkButton;

// TODO: work out why the obvious thing of having separate callbacks for each of the trial types doesn't work
// for the moment, all the work for all production trial types is done in the methods in ProductionTrial.as
private function productionVocabClickTyping(event:Event):void {
	QueuedSounds = [Sounds[event.currentTarget.label]];
	CurrentlyPlaying = -1;
	playSounds(0);
}

protected function typingTrial():void {
	var trial:Array = Trials[CurrentTrial];
	CurrentNetStream = Videos[trial[1]];
	CurrentNetStream2 = null;
	typingResponse.text = '';
	typingResponse.setFocus();
	setUpProductionVocab(new ArrayCollection(trial[2].split(" ")));
	ProductionAnswer = new ArrayCollection();
	QueuedSounds = [];
	// we don't want toggle buttons
	for(var lbi:int=0; lbi<productionVocab.length; lbi++) {
		var lb:LinkButton = productionVocab[lbi] as LinkButton;
		lb.toggle = false;
	}

	// check whether everything is loaded
	if(CurrentNetStream.bytesLoaded >= CurrentNetStream.bytesTotal-100) {
		typingTrialGo();
	} else {
		var prog:ProgressPopup =  new ProgressPopup();
		PopUpManager.addPopUp(prog, this, true);
		PopUpManager.centerPopUp(prog);
		// use a timer or something, or connect directly to netstream?
		PopupInterval = setInterval(checkPreload, 100, prog);
	}
//	continueButton.enabled = canContinue();
	replayButton.enabled = replayButton.visible; // since there's no sound playing to wait for

}

protected function typingTrialGo():void {
	var trial:Array = Trials[CurrentTrial];
	CurrentNetStream.seek(0);
	CurrentNetStream.resume();
}

protected function textTyped(event:Event):void {
	Answered = typingResponse.text != '';
}

