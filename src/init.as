// in this file goes all the initialization code for the applet, stuff that just gets run once when we load

import mx.collections.ArrayCollection;
import mx.controls.Image;
import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.utils.URLUtil;
import flash.events.*;
import flash.net.*;
import flash.media.Sound;
import flash.utils.Dictionary;

protected var VideoSource:String;
protected var SoundSource:String;
protected var ImageSource:String;
protected var VideosToPreload:Array;

private function startLoading(event:FlexEvent):void {
    /*
     Declared in allapp.mxml and initialized here:
     - Cookie
     Declared in allapp.mxml and referenced here:
     -
     */
	//protected var BasePath:String = "http://www.stanford.edu/~hjt/assets/webag/";
	//protected var BasePath:String = "http://langcog.stanford.edu/expts/webag/"
	var BasePath:String = "http://localhost:8080/mturk/";
	//var BasePath:String = "https://www.hlp.rochester.edu/mturk/WOACQ.1/";

	// choose a trial file from the server
	var flashVars:Object = FlexGlobals.topLevelApplication.parameters;
	var selfURL:String = FlexGlobals.topLevelApplication.url;
	var BaseURL:String = URLUtil.getProtocol(selfURL) + '://' + URLUtil.getServerNameWithPort(selfURL) + '/';
	trace('URL: ' + BaseURL);
	if(flashVars == null) {
		flashVars = [];
	}
	if(!flashVars.hasOwnProperty("script")) {
		flashVars["script"] = "testscript";
	}
	if(flashVars.hasOwnProperty("path")) {
		BasePath = BaseURL + flashVars["path"];
		trace('BasePath set to: ' + BasePath);
	}

	VideoSource = BasePath + "videos/";
	SoundSource = BasePath + "sounds/";
	ImageSource = BasePath + "images/";

	Security.loadPolicyFile(BasePath + "crossdomain.xml");
	// initialise the cookies
	Cookie = SharedObject.getLocal("WebAG");
	trace("Cookie size: " + Cookie.size);
	for (var prop:String in Cookie.data) {
		var obj:* = Cookie.data[prop];
		trace('\t' + prop + " = " + obj);
	}

	// here are some default settings
	setVariable("replayEnabled", "1");

	trace("FlashVars:");
	for(var i:String in flashVars) {
		trace('\t' + i + ": " + flashVars[i]);
	}
	var trialfile:String = BasePath + "trials/" + flashVars["script"];
	trace("Trial File is: " + trialfile);
	var loader:URLLoader = new URLLoader();
	loader.addEventListener(Event.COMPLETE, loadingComplete);
	loadingProgressBar.source = loader;
	loader.load(new URLRequest(trialfile));
}

private function loadingComplete(event:Event):void {
    /*
    Declared in allapp.mxml and initialized here:
            - Videos, Sounds, Images, ImagesToPreload, SoundsToPreload
    Declared in allapp.mxml and referenced here:
            -
    */
	var loader:URLLoader = URLLoader(event.target);
	processTrialFile(loader.data.split("\n"));
	var resNames:Array = extractResourceNames(Trials);
	Videos = new Dictionary();
	Sounds = new Dictionary();
	Images = new Dictionary();
	VideosToPreload = [];
	ImagesToPreload = [];
	SoundsToPreload = [];
	for each(var res:Array in resNames) {
		var resName:String = res[0];
		var resFile:String = res[1];
		var suffix:String = resFile.slice(-4, resFile.length);
		if(suffix == ".flv") {
			if(Videos.hasOwnProperty(resName))
				continue;
			var nc:NetConnection = new NetConnection();
			nc.connect(null);
			var ns:NetStream = new NetStream(nc);

			ns.client = { onMetaData:function(obj:Object):void{} };
			ns.addEventListener(AsyncErrorEvent.ASYNC_ERROR,
				function(evt:AsyncErrorEvent):void { }); //trace("async error: "+evt.text)
			// preload the first 60 seconds of video; VIDEOS LONGER THAN THIS WILL BREAK!
			ns.bufferTime = 60;
			VideosToPreload.push([resName, resFile]);
			Videos[resName] = ns;
		} else if(suffix == ".mp3") {
			// assert resFile.slice(-4, resFile.length) == ".mp3"
			if(Sounds.hasOwnProperty(resName))
				continue;
			var s:Sound = new Sound();
			// s.addEventListener(Event.COMPLETE, onSoundLoaded);
			var req:URLRequest = new URLRequest(resFile);
			SoundsToPreload.push([s, req]);
			Sounds[resName] = s;
		} else if(suffix == ".png"){
			if(Images.hasOwnProperty(resName)) continue;
			var i:Image = new Image();
			var picreq:URLRequest = new URLRequest(resFile);
			ImagesToPreload.push([i, picreq]);
			Images[resName] = i;
		} else {
			trace("Attempt to load resource of unsupported type: " + resFile);
		}
	}
	trace("Preparing to preload:");
	trace("\t" + VideosToPreload.length + " videos");
	trace("\t" + SoundsToPreload.length + " sounds");
	trace("\t" + ImagesToPreload.length + " images");
	// start preloading the first video
	var firstResName:String = VideosToPreload[0][0];
	var firstNS:NetStream = Videos[firstResName];
	firstNS.play(VideosToPreload[0][1]);
	firstNS.pause();
	trace("Preloading video: " + VideosToPreload[0][0] + "," + VideosToPreload[0][1]);
	// call the preload function later
	setTimeout(preloadVideoCallback, 200, VideosToPreload);

	// start loading the first sound file
	if(SoundsToPreload.length == 0) {
		// nothing to do
	} else {
		var firstSound:Sound = SoundsToPreload[0][0];
		trace("Preloading sound:" + SoundsToPreload[0][1].url);
		// call the preload again once this is done
		firstSound.addEventListener(Event.COMPLETE, preloadSoundCallback);
		firstSound.load(SoundsToPreload[0][1]);
	}

	// start loading the first image file
	if(ImagesToPreload.length == 0) {
		// nothing to do
	} else {
		var firstImage:Image = ImagesToPreload[0][0];
		trace("Preloading image: " + ImagesToPreload[0][0] + "," + ImagesToPreload[0][1].url);
		// call the preload again once this is done
		firstImage.addEventListener(Event.COMPLETE, preloadImageCallback);
		firstImage.load(ImagesToPreload[0][1].url);
	}

	// now start running through trials until we hit something that actually
	// means the experiment is beginning (we need to do this to deal with
	// any "set" commands in the preamble, etc
	while(Trials[CurrentTrial+1][0] == "set") {
		CurrentTrial++;
		setVariable(Trials[CurrentTrial][1], Trials[CurrentTrial][2])
	}

	// OK, all done -- go to menu
	currentState = "Menu";
}

private function preloadVideoCallback(VideosToPreload:Array):void {
	var ns:NetStream = Videos[VideosToPreload[0][0]];
	if(ns.bytesLoaded >= ns.bytesTotal-100) {
		// done, so go on to the next video
		VideosToPreload.shift();
		if(VideosToPreload.length == 0)
			return;
		ns = Videos[VideosToPreload[0][0]];
		ns.play(VideosToPreload[0][1]);
		trace("Preloading video: " + VideosToPreload[0][0] + "," + VideosToPreload[0][1]);
		ns.pause();
	}
	setTimeout(preloadVideoCallback, 200, VideosToPreload);
}

private function preloadSoundCallback(event:Event):void {
	SoundsToPreload.shift();
	if(SoundsToPreload.length > 0) {
		trace("Preloading sound: " + SoundsToPreload[0][1].url);
		var s:Sound = SoundsToPreload[0][0];
		s.addEventListener(Event.COMPLETE, preloadSoundCallback);
		s.load(SoundsToPreload[0][1])
	}
}

private function preloadImageCallback(event:Event):void {
	ImagesToPreload.shift();
	if(ImagesToPreload.length > 0) {
		trace("Preloading image: " + ImagesToPreload[0][0] + "," + ImagesToPreload[0][1].url);
		var i:Image = ImagesToPreload[0][0];
		i.addEventListener(Event.COMPLETE, preloadImageCallback);
		i.load(ImagesToPreload[0][1].url)
	}
}

private function processTrialFile(lines:Array):void {
	/*
    Declared in allapp.mxml and initialized here:
     - Trials, SectionNames, SectionStarts, SectionEnds, DisplayedTrialNo
    Declared in allapp.mxml and referenced here:
     - Labels
     */
    Trials = [];
    SectionNames = [];
    SectionEnds = new ArrayCollection();
    SectionStarts = new ArrayCollection();

    var line:String;
	for each(line in lines) {
		line = StringUtil.trim(line);
		if(line == "" || line.substr(0, 1) == "#")
			continue;
		var l:Array = line.split("\t");
		if(l[0] == "sec") {
			SectionNames.push(l[1])
			SectionStarts.addItem(Trials.length)
		} else if (l[0] == "end") {
			SectionEnds.addItem(Trials.length);
		} else if (l[0] == "label") {
			Labels[l[1]] = Trials.length;
			trace("label:" + l[1] + ":" + Trials.length);
		} else {
			Trials.push(l);
		}
	}
	var actualTrial:int = 0;
	DisplayedTrialNo = new Dictionary();
	for(var i:int=0; i < Trials.length; i++) {
		if(isActualTrialType(Trials[i][0])) {
			actualTrial++;
		}
		DisplayedTrialNo[i] = actualTrial;
	}
}

private function extractResourceNames(trials:Array):Array {
    var ret:Array = [];
    var word:String;
    for each(var n:Array in trials) {
        switch(n[0]) {
            case "exp":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                for each(word in n[2].split(" ")) {
                    ret.push([word, SoundSource + word + ".mp3"]);
                }
                break;
            case "discrim":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                ret.push([n[2], VideoSource + n[2] + ".flv"]);
                for each(word in n[3].split(" ")) {
                    ret.push([word, SoundSource + word + ".mp3"]);
                }
                break;
            case "prod": /* fall through to "cuedprod" */
            case "cuedprod":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                for each(word in n[2].split(" ")) {
                    ret.push([word, SoundSource + word + ".mp3"]);
                }
                break;
            case "prod1":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                for each(word in n[2].split(" ")) {
                    ret.push([word, SoundSource + word + ".mp3"]);
                }
                break;
            case "type":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                for each(word in n[2].split(" ")) {
                    ret.push([word, SoundSource + word + ".mp3"]);
                }
                break;
            case "typeprompt":
                ret.push([n[1], VideoSource + n[1] + ".flv"]);
                break;
            case "4pic":
                ret.push([n[1], ImageSource + n[1] + ".png"]);
                ret.push([n[2], ImageSource + n[2] + ".png"]);
                ret.push([n[3], ImageSource + n[3] + ".png"]);
                ret.push([n[4], ImageSource + n[4] + ".png"]);
                ret.push([n[5], SoundSource + n[5] + ".mp3"]);
                break;
            default:
                trace("Unknown trial type: " + n[0]);
                break;
        }
    }
    return ret;
}


